<?php

namespace Haven\Preview;

use Illuminate\Routing\Router;
use Illuminate\Support\ServiceProvider;

class PreviewServiceProvider extends ServiceProvider
{
    /**
     * Perform post-registration booting of services.
     *
     * @return void
     */
    public function boot(Router $router)
    {
        $this->loadViewsFrom(__DIR__ . '/views', 'preview');

        $this->publishes([
            __DIR__ . '/preview.php' => config_path('preview.php')
        ]);

        $router->aliasMiddleware('preview', \Haven\Preview\PreviewMiddleware::class);

        $this->loadRoutesFrom(__DIR__ . '/routes.php');
    }

    /**
     * Register any package services.
     *
     * @return void
     */
    public function register()
    {
        //
    }
}