<!DOCTYPE html>
<html>
<head>
    <title>{{ $name }}</title>
</head>
<body>
    <h4>This is an example page!!</h4>

    <p>Hello, {{ $name }}</p>
</body>
</html>