<!DOCTYPE html>
<html>
<head>
    <title>Available Previews</title>
</head>
<body>
    <h3>Available Previews:</h3>

    <ul>
        @foreach ($previews as $preview)
        <li><a href="{{ url('preview/'.$preview['route']) }}" target="_blank">{{ $preview['name'] }}</a></li>
        @endforeach
    </ul>

    <p>Note that the functionality and hyperlinks on these pages probably won't work. This is intentional.</p>
    <p><strong>IMPORTANT</strong>: Various previews have to force short-term session variables in order to simulate certain states. Sometimes these variables can persist an extra request, so if a preview page looks weird, just hit refresh once or twice.</p>
</body>
</html>