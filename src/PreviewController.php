<?php

namespace Haven\Preview;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;

class PreviewController extends Controller
{
    private $view;

    public function index()
    {
        return view('preview::list')->with([
            'previews' => config('preview')
        ]);
    }

    public function viewer($route)
    {
        $preview = collect(collect(config('preview'))->where('route', $route)->first());

        if ($preview->isEmpty()) {
            abort(404);
        }

        $this->createView($preview->get('view'));

        $this->setData($preview->get('data'));

        $this->setFlash($preview->get('flash'));

        return $this->view;
    }

    protected function setData($data)
    {
        foreach($data as $k => $v) {
            if (is_callable($v)) {
                $v = $v();
            }

            $this->view->with($k, $v);
        }
    }

    protected function setFlash($flash)
    {
        foreach($flash as $k => $v) {
            request()->session()->flash($k, $v);
        }
    }

    protected function createView($view)
    {
        $this->view = view($view)
            ->with('disable_forms', true);
    }

}
