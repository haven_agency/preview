<?php

return [
    [
        'name' => 'Example',
        'route' => 'example_page',
        'view' => 'preview::example',
        'data' => ['name' => 'Bob'],
        'flash' => []
    ],

    // ...
];