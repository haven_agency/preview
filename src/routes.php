<?php

Route::group(['middleware' => 'preview'], function () {
    Route::get('preview', 'Haven\Preview\PreviewController@index');
    Route::get('preview/{page}', 'Haven\Preview\PreviewController@viewer');
});