# Preview

Preview allows the client to preview the user flow of the application.


## Installation

Add Preview repo to `composer.json`:
```
"repositories": [{
    "type": "vcs",
    "url": "https://bitbucket.org/haven_agency/preview.git"
}]
```

Run: 
```bash
$ composer require "haven/preview:v1.2.*"
```


Run the following command to create `config/preview.php`:
```bash
$ php artisan vendor:publish --provider="Haven\Preview\PreviewServiceProvider"
```

Add the following script to the main blade layout template:
```
@if (!empty($disable_forms))
<!-- This is a preview page; prevent form submissions -->
<script>
var previewMode = true;

// Delete if not using jQuery
$(document).ready(function() {
    $('form').on('submit', function() {
        alert('This is a preview page; form submissions are disabled.');
        return false;
    });
});
</script>
@endif
```

Use JS to prevent any form submit events by comparing the `previewMode` variable


## Change log

Please see [CHANGELOG](CHANGELOG.md) for more information on what has changed recently.


## Contributing

Please see [CONTRIBUTING](CONTRIBUTING.md) and [CONDUCT](CONDUCT.md) for details.

## Security

If you discover any security related issues, please email dev@havenagency.com instead of using the issue tracker.

## License

The MIT License (MIT). Please see [License File](LICENSE.md) for more information.
